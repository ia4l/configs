#!/usr/bin/env bash

CONFIG_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Adding vim stuff"
mkdir -p ~/.vim/colors
ln -sf $(readlink -f ${CONFIG_DIR}/.vimrc) ~/.vimrc
ln -sf $(readlink -f ${CONFIG_DIR}/molokai.vim) ~/.vim/colors

echo "Adding tmux stuff"
ln -sf $(readlink -f ${CONFIG_DIR}/.tmux.conf) ~/.tmux.conf

# Bash stuff
if [ -f "${HOME}/.bashrc" ]; then
  echo "Adding Bash-specific stuff"
  echo "source ${CONFIG_DIR}/aliases.bash" >> ${HOME}/.bashrc
fi

# Git stuff
if [ `which git` ]; then
  echo "Adding git settings"
  source ${CONFIG_DIR}/git_settings.sh
else
  echo "No git found in PATH"
fi

echo "Copying files to ${HOME}/bin"
mkdir -p ${HOME}/bin
ln -sf ${CONFIG_DIR}/bin/* ${HOME}/bin
