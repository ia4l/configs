set tabstop=2
set shiftwidth=2
set expandtab
set softtabstop=2
set history=2000
set ttyfast
set scrolljump=1

if has('gui_running')
    " GUI colors
    colorscheme molokai
else
    " Non-GUI (terminal) colors
    colorscheme desert
endif

set ai " autoindent
set enc=utf-8
set fenc=utf-8
set smartindent " C-only?
set number
"set cursorline

set modeline

"set statusline=%M%h%y\ %t\ %F\ %p%%\ %l/%L\ %=[%{&ff},%{&ft}]\ [a=\%03.3b]\ [h=\%02.2B]\ [%l,%v]

set statusline=
set statusline+=%7*\[%n]                                  "buffernr
set statusline+=%1*\ %<%F\                                "File+path
set statusline+=%2*\ %y\                                  "FileType
set statusline+=%3*\ %{''.(&fenc!=''?&fenc:&enc).''}      "Encoding
set statusline+=%3*\ %{(&bomb?\",BOM\":\"\")}\            "Encoding2
"set statusline+=%4*\ %{&ff}\                              "FileFormat (dos/unix..) 
"set statusline+=%5*\ %{&spelllang}\%{HighlightSearch()}\  "Spellanguage & Highlight on?
set statusline+=%8*\ %=\ row:%l/%L\ (%03p%%)\             "Rownumber/total (%)
set statusline+=%9*\ col:%03c\                            "Colnr
set statusline+=%0*\ \ %m%r%w\ %P\ \                      "Modified? Readonly? Top/bot.

set laststatus=2 "show statusline
set ignorecase
set smartcase

"This is somewhat OS-dependent
"set guifont=Anonymous:h10
set guifont=Bitstream\ Vera\ Sans\ Mono\ 10.


set ruler

set bs=2 " backspace over indents
set t_Co=256

syntax on

"set lines=45
"set columns=100

set hlsearch "highlighting search
set incsearch

set mouse=a
set showmatch "show matching brackets

map <C-t> :tabnew<cr>
map <C-left> :tabprevious<cr>
map <C-right> :tabnext<cr>

au! BufWritePost ~/.vimrc source %


function! HighlightSearch()
  if &hls
    return 'H'
  else
    return ''
  endif
endfunction
" au GUIEnter * simalt ~x "x on an English Windows.
"
"call pathogen#infect()
"filetype plugin indent on

" Initial size
"if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window.
"  set lines=999 columns=999
"else
  " This is console Vim.
"  if exists("+lines")
"    set lines=50
"  endif
"  if exists("+columns")
"    set columns=100
"  endif
"endif

" set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
"set guioptions-=r  "remove right-hand scroll bar

" call pathogen#infect()

" If the current buffer has never been saved, it will have no name,
" call the file browser to save it, otherwise just save it.
"nnoremap <silent> <C-S> :if expand("%") == ""<CR>browse confirm w<CR>else<CR>confirm w<CR>endif<CR>

nmap <F2> :update<CR>
vmap <F2> <Esc><F2>gv
imap <F2> <c-o><F2>

" unbind F1 (help)
:nmap <F1> :echo<CR>
:imap <F1> <C-o>:echo<CR>

map <F4> :!pdflatex %<CR>
"map <F5> :!bibtex %:r<CR>
map <F3> :tab drop %:p:s,.h$,.X123X,:s,.cc$,.h,:s,.X123X$,.cc,<CR>

" Toggle 80 warning
hi colorcolumn ctermbg=7
fun! ToggleCC()
  if &cc== ''
    set cc=80
  else
    set cc=
  endif
  set cc?
endfun

nnoremap <F9> :set spell!<CR>:set spell?<CR>
nnoremap <F10> :set list!<CR>:set list?<CR>
nnoremap <F11> :call ToggleCC()<CR>


hi User1 guifg=#ffdad8  guibg=#880c0e
hi User2 guifg=#000000  guibg=#F4905C
hi User3 guifg=#292b00  guibg=#f4f597
hi User4 guifg=#112605  guibg=#aefe7B
hi User5 guifg=#051d00  guibg=#7dcc7d
hi User7 guifg=#ffffff  guibg=#880c0e gui=bold
hi User8 guifg=#ffffff  guibg=#5b7fbb
hi User9 guifg=#ffffff  guibg=#810085
hi User0 guifg=#ffffff  guibg=#094afe
